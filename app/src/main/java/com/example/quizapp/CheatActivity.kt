package com.example.quizapp

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.cheat_activity.*

class CheatActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.cheat_activity)

        val answers: IntArray = intArrayOf(1,1,2,1,2)
       val position = getIntent().getIntExtra("position",1)
        cheatAnswer.visibility= View.GONE


        show_cheat.setOnClickListener(){
            if(answers[position]==1){
            cheatAnswer.setText("True")
            }else{
            cheatAnswer.setText("False")
            }
            cheatAnswer.visibility= View.VISIBLE

        }

        back.setOnClickListener(){
            finish()
        }

    }

}