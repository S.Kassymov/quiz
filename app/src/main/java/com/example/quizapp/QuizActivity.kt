package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.activity_quiz.*

class QuizActivity : AppCompatActivity() {
    private var curPosition: Int = 1
    private var questions: ArrayList<QuestionModel>? = null
    private var selPosition: Int = 0
    private var numOfCorAns: Int = 0
    private var useHint:Int=0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        questions = getQuestions()
        setQuestion()



        show_answer.setOnClickListener {
            if (useHint < 3) {
                    useHint++
                    val intent1 = Intent(this, CheatActivity::class.java)
                    intent1.putExtra("position",curPosition)
                    startActivity(intent1)

            } else {
                Toast.makeText(this, "You used all cheat chances", Toast.LENGTH_SHORT).show()
            }

        }

        trueOption.setOnClickListener {
            selPosition = 1
            if(curPosition==5){
                val intent2 = Intent(this, ResultActivity::class.java)
                intent2.putExtra("result",numOfCorAns)
                startActivity(intent2)
            }else {
                nextQuestion()
            }        }

        falseOption.setOnClickListener {
            selPosition = 2
            if(curPosition==5){
                val intent2 = Intent(this, ResultActivity::class.java)
                intent2.putExtra("result",numOfCorAns)
                startActivity(intent2)
            }else{
            nextQuestion()
        }}


    }

    private fun nextQuestion() {
        val question = questions?.get(curPosition - 1)

        if (question!!.correctAnswer == selPosition) {
            numOfCorAns++
        }
        curPosition++
        setQuestion()
    }

    private fun setQuestion() {
        val question =
                questions!!.get(curPosition - 1)
        questionText.text = question.question
    }

    private fun getQuestions(): ArrayList<QuestionModel> {
        val questionList = ArrayList<QuestionModel>()

        val q1 = QuestionModel(
                1,
                "DMC 5 features four playable characters",
                1,
                "True."

        )

        val q2 = QuestionModel(
                2,
                "From 2019 Sony announced that they would not participate in E3 conference",
                1,
                "True."

        )

        val q3 = QuestionModel(
                3,
                "Half-Life 2:Episode two  currently has the most ratings in metacritic",
                2,
                "False."
        )

        val q4 = QuestionModel(
                4,
                "The new PlayStation 5 and Xbox:Series X/S is a nineth generation consoles",
                1,
                "True."
        )
        val q5 = QuestionModel(
                5,
                "Metal Gear Rising: Revengeance was debuted in 2009",
                2,
                "False."
        )

        questionList.add(q1)
        questionList.add(q2)
        questionList.add(q3)
        questionList.add(q4)
        questionList.add(q5)
        return questionList

    }

    data class QuestionModel(    val id: Int,
                                 val question: String,
                                 val correctAnswer: Int,
                                 val hint: String
    )

}
